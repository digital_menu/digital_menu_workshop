import React from 'react'
import { useCookies } from 'react-cookie'
import { Button } from '@material-ui/core'
import { useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { deleteDishes } from '../../services' 
import { Delete  } from '@material-ui/icons'
import './deleting-modal.scss'

export const DeletingModal = ({ menu_id, section_id, value, type, setisDeleting, list, setDeletion }:any) => {
  const [cookies] = useCookies(['digital_menu_user'])
  let { restaurant_id } = useParams()
  const { t } = useTranslation()

  const deleteMe = () => {
    if(type === "Platillo"){
      deleteDishes(cookies.digital_menu_user.id, restaurant_id, menu_id, section_id, value.id)
      .then(()=> setDeletion(list.filter((item:any) => item.id !== value.id)))
      setisDeleting(false)
    }
  }
  return(
    <div className="modal__container">
      <span>
        {t('DELETE_DIALOG_TITLE')} {type.toLowerCase()}
      </span>
      <br/>
      <span>
        {t('DELETE_DIALOG_DESCRIPTION')}
      </span>
      <div className="modal__variable">
        <span>
          {`${type}: "${value.name}"`}
        </span>
      </div>
      <div className="modal__actions">
        <Button onClick={() => setisDeleting(false)}>
          <span>
            {t('CANCEL_BUTTON')}
          </span>
        </Button>
        <Button style={{ color: '#f44336' }} endIcon={<Delete />} onClick={deleteMe}>
          <span>
            {t('DELETE_BUTTON')}
          </span>
        </Button>
      </div>
    </div>
  )
}