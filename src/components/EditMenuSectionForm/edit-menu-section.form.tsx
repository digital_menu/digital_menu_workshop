import React, { useState } from 'react'
import { Add, PhotoCamera } from '@material-ui/icons'
import { TextField, Button } from '@material-ui/core'

import { useTranslation } from 'react-i18next';

export const Form = ({ values, errors, handleChange, handleSubmit, setFieldValue }:any) => {
  const [Image, setImage] = useState('')
  const { t } = useTranslation()
  return(
    <div className="form-container">
      <div className="restaurant-form">
        <div className="restaurant-form__section">
          <TextField 
            value={values.name}
            helperText={errors.name}
            onChange={handleChange('name')}
            label={t('SECTIONFORM_SECTION_NAME')} 
            error={errors.name? true : false }
          />
          <TextField 
            value={values.description}
            helperText={errors.description}
            onChange={handleChange('description')}
            label={t('SECTIONFORM_SECTION_DESCRIPTION')} 
            error={errors.name? true : false }
          />
        </div>
        <div className="restaurant-form__section">
          <input accept="image/*" id="menu-section-upload" type="file" onChange={(event) => {
          let reader = new FileReader();
          // @ts-ignore
          reader.readAsDataURL(event.currentTarget.files[0]);
          reader.onload = () => {
            // @ts-ignore
            setFieldValue("image", reader.result);
            // @ts-ignore
            setImage(reader.result)
          }
          reader.onerror = (error) => {
            console.log('Error: ', error);
          }
          }} />
          <label htmlFor="menu-section-upload">
            <Button endIcon={<PhotoCamera />} component="span">
              {t('FORM_IMAGEBUTTON')}
            </Button>
          </label>
          {Image !== ''? 
            <img alt="menu-section" src={Image} width={200} height={200} />:
            null}
        </div>
      </div>
      <div className="submitButton">
        <Button 
          endIcon={<Add />}
          onClick={handleSubmit} 
        >
          {t('SECTIONFORM_SECTION_BUTTON')} 
        </Button>
      </div>
    </div>
  )
}