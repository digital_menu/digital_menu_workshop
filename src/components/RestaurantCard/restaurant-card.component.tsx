import { Card, CardContent, CardActions, Typography, Button, IconButton, Dialog, Tooltip } from '@material-ui/core'
import { Delete, GetApp, MoreVert, Settings } from '@material-ui/icons'
import { useHistory, useRouteMatch } from 'react-router-dom';
import { useTranslation } from 'react-i18next'
import React, { useState } from 'react'
import QRCode from 'qrcode.react'

import { DeletingModal } from '../../components'
import { SettingsModal } from '../SettingsModal';

import './restaurant-card.scss'

export const RestaurantCard = ({ value }:any) => {
  const [isSettings, setisSettings] = useState(false)
  const [isDeleting, setisDeleting] = useState(false)
  const anchorRef = React.useRef(null);
  const { url } = useRouteMatch()
  const { t } = useTranslation()
  const history = useHistory()

  return(
    <Card className="card" variant="outlined">
      <CardContent>
        <Typography color="textPrimary" gutterBottom>
          {value.address}
        </Typography>
        <Typography color="primary" variant="h5" component="h2">
          {value.name.replace('-', ' ')}
        </Typography>
        <Typography color="textSecondary">
          {value.enabled? "Menu activo":"Menu Inactivo"}
        </Typography>
        <Typography color="secondary" variant="body2" component="p">
          {value.description}
        </Typography>
        <div>
          <QRCode size={160} includeMargin={true} value={`http://192.168.1.85:3001/digital-menu/${value.name}`} />
        </div>
      </CardContent>
      <CardActions className="card__actions" >
        <Button onClick={() => history.push(`${url}/${value.id}/menu-workshop`)} >
          {t('RESTAURANT_CARD_MENUBUTTON')}
        </Button>
        <Tooltip title="Descargar QR" aria-label="add">
          <IconButton color="primary" ref={anchorRef} aria-label="share">
            <GetApp />
          </IconButton>
        </Tooltip>
        <IconButton color="primary" onClick={() => setisDeleting(true)}>
          <Delete />
        </IconButton>
        <IconButton color="primary" onClick={() => setisSettings(true)}>
          <Settings />
        </IconButton>
        <IconButton color="primary" aria-label="share">
          <MoreVert />
        </IconButton>
      </CardActions>
      <Dialog
        open={isDeleting}
        onClose={() => setisDeleting(false)}
      >
        <DeletingModal value={value} setisDeleting={setisDeleting} type="Restaurante" />
      </Dialog>
      <Dialog
        open={isSettings}
        onClose={() => setisSettings(false)}
      >
        <SettingsModal restaurant_id={value.id} />
      </Dialog>
    </Card>
  )
}