import React from 'react'
import { Formik } from 'formik'
import { useCookies } from 'react-cookie'
import { useHistory } from 'react-router-dom'

import { Form } from './login.form'
import { loginUser } from '../../services'
import { Validations } from './validations'

type InitialValues = {
  username: string
  password: string
}

const initialValues: InitialValues = {
  username: '',
  password: ''
} 

export const LoginForm = () => {
  const history = useHistory()
  const setCookie = useCookies(['digital_menu_user'])[1]

  const onSubmit = (values: InitialValues, { resetForm }:any) => {
    loginUser({credentials: values})
    .then(res => {
      setCookie('digital_menu_user', res.data.data, { path: '/' })
      history.push('dashboard')
    })
    .catch(err=>console.log(err))
    resetForm()
  }
  return(
    <Formik
      validationSchema={Validations}
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      {({
         values,
         errors,
         handleChange,
         handleSubmit,
         isSubmitting,
       }) => (
        <Form 
          values={values} 
          errors={errors} 
          handleChange={handleChange} 
          handleSubmit={handleSubmit} />
        )
      }
    </Formik>
  )
}