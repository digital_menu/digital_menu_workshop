import * as Yup from 'yup'
import i18n from 'i18next'

export const Validations = Yup.object({
  username: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED')),
  password: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED')),
})