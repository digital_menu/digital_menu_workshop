import React from 'react'
import { TextField, Button } from '@material-ui/core'
import { SupervisedUserCircle } from '@material-ui/icons'
import { useTranslation } from 'react-i18next';

import './login.scss'

export const Form = ({ values, errors, handleChange, handleSubmit }:any) => {
  const { t } = useTranslation()
  return(
    <form onSubmit={handleSubmit} className="loginform">
      <TextField
        value={values.username}
        helperText={errors.username}
        onChange={handleChange('username')}
        label={t('LOGIN_USERNAME')}
        error={errors.username? true : false }
      />
      <TextField
        value={values.password}
        helperText={errors.password}
        onChange={handleChange('password')}
        label={t('LOGIN_PASSWORD')}
        error={errors.password? true : false }
      />
      <Button
        type="submit"
        endIcon={<SupervisedUserCircle />}
        onClick={handleSubmit}
      >
        {t('LOGIN_BUTTON')}
      </Button>
    </form>
  )
}
