import * as Yup from 'yup'
import i18n from 'i18next'

export const Validations = Yup.object({
  name: Yup.string()
    .max(30, i18n.t('FORMFIELD_30_LENGTH'))
    .required(i18n.t('FORMFIELD_REQUIRED')),
  description: Yup.string()
    .max(70, i18n.t('FORMFIELD_70_LENGTH'))
    .required(i18n.t('FORMFIELD_REQUIRED')),
  price: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED')),
})