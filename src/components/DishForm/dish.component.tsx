import React from 'react'
import { Formik } from 'formik'
import { useCookies } from 'react-cookie'
import { useParams } from 'react-router-dom'

import { Form } from './dish.form'
import { addDishes } from '../../services'
import { Validations } from './validations'

type InitialValues = {
  name: string
  price: string
  description: string
}

const initialValues: InitialValues = {
  name: '',
  price: '',
  description: ''
} 

export const DishForm = ({ menu_id, section_id, setDishes }: any) => {
  let { restaurant_id } = useParams();
  const [cookies] = useCookies(['digital_menu_user']);
  const onSubmit = (values: InitialValues, { resetForm }:any) => {
    addDishes(cookies.digital_menu_user.id, restaurant_id, menu_id, section_id, {dishes: values})
    .then(res => setDishes((state:any) => [...state, res.data.data ]))
    resetForm()
  }
  return(
    <Formik
      validationSchema={Validations}
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      {({
         values,
         errors,
         handleChange,
         handleSubmit,
         isSubmitting,
         setFieldValue
       }) => (
        <Form 
          values={values} 
          errors={errors} 
          handleChange={handleChange} 
          handleSubmit={handleSubmit}
          setFieldValue={setFieldValue} />
        )
      }
    </Formik>
  )
}