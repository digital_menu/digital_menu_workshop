
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next';
import { Add, PhotoCamera } from '@material-ui/icons'
import { TextField, Button } from '@material-ui/core'

import './dish.scss'

export const Form = ({ values, errors, handleChange, handleSubmit, setFieldValue }:any) => {
  const [Image, setImage] = useState('')
  const { t } = useTranslation()
  return(
    <div className="form-container">
      <div className="restaurant-form">
        <div className="restaurant-form__section">
          <TextField
            value={values.name}
            helperText={errors.name}
            onChange={handleChange('name')}
            label={t('DISHFORM_DISH_NAME')}
            error={errors.name? true : false }
          />
          <TextField
            rows={3}
            multiline={true}
            value={values.description}
            helperText={errors.description}
            onChange={handleChange('description')}
            label={t('DISHFORM_DISH_DESCRIPTION')}
            error={errors.description? true : false }
            className="form-container__textarea"
          />
          <TextField
            value={values.price}
            helperText={errors.price}
            onChange={handleChange('price')}
            label={t('DISHFORM_DISH_PRICE')}
            error={errors.price? true : false }
          />
        </div>
        <div className="restaurant-form__section">
          <input accept="image/*" id="menu-dishes" type="file" onChange={(event) => {
            let reader = new FileReader();
            // @ts-ignore
            reader.readAsDataURL(event.currentTarget.files[0]);
            reader.onload = () => {
              // @ts-ignore
              setImage(reader.result)
              // @ts-ignore
              setFieldValue("image", reader.result);
            }
            reader.onerror = (error) => {
              console.log('Error: ', error);
            }
          }} />
          <label htmlFor="menu-dishes">
            <Button endIcon={<PhotoCamera />} component="span">
              {t('FORM_IMAGEBUTTON')}
            </Button>
          </label>
          {Image !== ''?
            <img alt="menu-dish" src={Image} width={200} height={200} />:
            null}
        </div>
      </div>
      <div className="submitButton">
        <Button
          endIcon={<Add />}
          onClick={handleSubmit}
        >
          {t('DISHFORM_DISH_BUTTON')}
        </Button>
      </div>
    </div>
  )
}
