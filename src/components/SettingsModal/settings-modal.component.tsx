import React, { useEffect, useState } from 'react'
import { Formik } from 'formik'
import { useCookies } from 'react-cookie'

import { Form } from './settings-modal.form'
import { Validations } from './validations'
import { updateSettings, getSettings } from '../../services'

type InitialValues = {
    id: number
    font_type?: string
    promot_timer?: string
    main_color: string
    secondary_color: string
    text_color: string 
    secondary_text_color: string
}
  
const initialValues: InitialValues = {
    id: -1,
    main_color: '#fff',
    secondary_color: '#fff',
    text_color: '#000',
    secondary_text_color: '#000'
} 


export const SettingsModal = ({ restaurant_id }: any) => {
    const [cookies] = useCookies(['digital_menu_user'])
    const [isLoading, setisLoading] =  useState(false)
    const [Settings, setSettings] =  useState(initialValues)
    
    const onSubmit = (values: InitialValues, { resetForm }:any) => {
        updateSettings(cookies.digital_menu_user.id, restaurant_id, Settings.id, { setting: values })
        .then(() => setSettings(values))
        .catch(err => console.log(err))
    }
    useEffect(() => {
        setisLoading(true)
        getSettings(cookies.digital_menu_user.id, restaurant_id)
        .then(res =>{
            setSettings(res.data.data[0])
            setisLoading(false)
        })
        .catch(() => setisLoading(false))
    }, [cookies.digital_menu_user.id, restaurant_id])
    return !isLoading?(
        <Formik
            validationSchema={Validations}
            initialValues={Settings}
            onSubmit={onSubmit}
        >
            {({
                values,
                errors,
                handleChange,
                handleSubmit,
                isSubmitting,
                setFieldValue
                }) => (
                <Form 
                values={values} 
                errors={errors} 
                handleChange={handleChange} 
                handleSubmit={handleSubmit}
                setFieldValue={setFieldValue} />
                )
            }
        </Formik>
    ):null
}