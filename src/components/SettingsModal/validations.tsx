import * as Yup from 'yup'
import i18n from 'i18next'

export const Validations = Yup.object({
  main_color: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED')),
  secondary_color: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED')),
  text_color: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED'))
})