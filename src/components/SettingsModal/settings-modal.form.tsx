import React from 'react'
import { Add } from '@material-ui/icons'
import { BlockPicker } from 'react-color';
import { TextField, Button } from '@material-ui/core'

import { useTranslation } from 'react-i18next';

export const Form = ({ values, errors, handleChange, handleSubmit, setFieldValue }:any) => {
  const { t } = useTranslation()
  return(
    <div className="form-container">
      <div className="restaurant-form">
        <div className="restaurant-form__section">
          <TextField 
            value={values.main_color}
            helperText={errors.main_color}
            onChange={handleChange('main_color')}
            label={t('SECTIONFORM_SECTION_NAME')} 
            error={errors.main_color? true : false }
          />
          <BlockPicker 
            color={values.main_color} 
            onChangeComplete={(color) => setFieldValue('main_color', color.hex)}/>
          <TextField 
            value={values.secondary_color}
            helperText={errors.secondary_color}
            onChange={handleChange('secondary_color')}
            label={t('SECTIONFORM_SECTION_NAME')} 
            error={errors.secondary_color? true : false }
          />
          <BlockPicker 
            color={values.secondary_color} 
            onChangeComplete={(color) => setFieldValue('secondary_color', color.hex)}/>
        </div>
        <div className="restaurant-form__section">
          <TextField 
            value={values.text_color}
            helperText={errors.text_color}
            onChange={handleChange('text_color')}
            label={t('SECTIONFORM_SECTION_DESCRIPTION')} 
            error={errors.text_color? true : false }
          />
          <BlockPicker 
            color={values.text_color} 
            onChangeComplete={(color) => setFieldValue('text_color', color.hex)}/>
          <TextField 
            value={values.secondary_text_color}
            helperText={errors.secondary_text_color}
            onChange={handleChange('secondary_text_color')}
            label={t('SECTIONFORM_SECTION_DESCRIPTION')} 
            error={errors.secondary_text_color? true : false }
          />
          <BlockPicker 
            color={values.secondary_text_color} 
            onChangeComplete={(color) => setFieldValue('secondary_text_color', color.hex)}/>
        </div>
      </div>
      <div className="submitButton">
        <Button 
          endIcon={<Add />}
          onClick={handleSubmit} 
        >
          {t('SECTIONFORM_SECTION_BUTTON')} 
        </Button>
      </div>
    </div>
  )
}