import React, { useState } from 'react'
import { Formik } from 'formik'
import { useCookies } from 'react-cookie'
import { useParams } from 'react-router-dom'

import { Form } from './menu.form'
import { addMenu } from '../../services'
import { Validations } from './validations'

type InitialValues = {
  name: string
  type: string
  image: any
  schedule: Array<String>
}

const initialValues: InitialValues = {
  name: '',
  type: '',
  image: null,
  schedule: []
} 

export const MenuForm = ({ setisOpen, setMenus }:any) => {
  let { restaurant_id } = useParams()
  const [cookies] = useCookies(['digital_menu_user'])
  const [isLoading, setisLoading] = useState(false)

  const onSubmit = (values: InitialValues, { resetForm }:any) => {
    addMenu(cookies.digital_menu_user.id, restaurant_id, {menu: values})
    .then((res: any) => {
      setMenus((state:any) => [...state, res.data.data])
      setisLoading(false)
      setisOpen(false)
    })
    .catch(err=>{
      setisLoading(false)
      setisOpen(false)
    })
    resetForm()
  }
  return(
    <Formik
      validationSchema={Validations}
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      {({
        values,
        errors,
        isValid,
        handleChange,
        handleSubmit,
        isSubmitting,
        setFieldValue,
      }) => (
        <Form 
          values={values} 
          errors={errors} 
          isValid={isValid}
          setisOpen={setisOpen}
          isLoading={isLoading}
          handleChange={handleChange} 
          handleSubmit={handleSubmit}
          setFieldValue={setFieldValue} />
        )
      }
    </Formik>
  )
}