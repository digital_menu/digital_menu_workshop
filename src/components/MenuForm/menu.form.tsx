import { TextField, Button, Select, FormControl, MenuItem, InputLabel, FormHelperText, CircularProgress } from '@material-ui/core'
import { Add, PhotoCamera } from '@material-ui/icons'
import { useTranslation } from 'react-i18next';
import React, { useState } from 'react'

import './menu.scss'

export const Form = ({ values, errors, handleChange, handleSubmit, setFieldValue, isLoading, isValid, setisOpen }:any) => {
  const [Image, setImage] = useState('')
  const { t } = useTranslation()
  return(
    <form className="form-container">
      <div className="restaurant-form">
        <div className="restaurant-form__section">
          <TextField 
            value={values.name}
            helperText={errors.name}
            onChange={handleChange('name')}
            label={t('MENU_FORM_NAMEFIELD')} 
            error={errors.name? true : false }
          />
          <FormControl 
            className="menu__select"
            error={errors.type? true : false }
          >
            <InputLabel>{t('MENU_FORM_TYPEFIELD')}</InputLabel>
            <Select
              value={values.type}
              onChange={handleChange('type')}
            >
              <MenuItem value="">
                {t('FORM_OPTIONSELECTION')}
              </MenuItem>
              <MenuItem value="Comida">Comida</MenuItem>
              <MenuItem value="Bebida">Bebida</MenuItem>
            </Select>
            <FormHelperText>{errors.type}</FormHelperText>
          </FormControl>
        </div>
        <div className="restaurant-form__section">
          <input accept="image/*" id="menu-upload" type="file" onChange={(event) => {
            let reader = new FileReader();
            // @ts-ignore
            reader.readAsDataURL(event.currentTarget.files[0]);
            reader.onload = () => {
              // @ts-ignore
              setFieldValue("image", reader.result);
              // @ts-ignore
              setImage(reader.result)
            }
            reader.onerror = (error) => {
              console.log('Error: ', error);
            }
            event.target.value = ''
          }} />
          <label htmlFor="menu-upload">
            <Button color="primary" endIcon={<PhotoCamera />} component="span">
              {t('FORM_IMAGEBUTTON')}
            </Button>
          </label>
          {Image !== ''? 
            <div className="form-container__image-wrap">
              <span>&times;</span>
              <img alt="restaurant" src={Image} width={'100%'} />
            </div>
          :null}
        </div>
      </div>
      <div className="submitButton">
        {
          isLoading?
          <CircularProgress />:null
        }
        <Button
          color="primary" 
          endIcon={<Add />}
          disabled={isLoading || !isValid}
          onClick={
            ()=>{
              setImage('')
              handleSubmit()
            }}
        >
          {t('MENU_FORM_SUBMITBUTTON')}
        </Button>
        <Button style={{ color: '#f44336' }} onClick={()=> setisOpen(false)}>
          {t('CANCEL_BUTTON')}
        </Button>
      </div>
    </form>
  )
}