import React from 'react'
import { Formik } from 'formik'
import { useCookies } from 'react-cookie'
import { useParams } from 'react-router-dom'

import { Form } from './edit-menu.form'
import { editMenu } from '../../services'
import { Validations } from './validations'

type InitialValues = {
  id: number
  name: string
  type: string
  image: any
  schedule: Array<String>
}

export const EditMenuForm = ({ values, index, Menus, setMenus }:any) => {
  console.log(values)
  let { restaurant_id } = useParams()
  const [cookies] = useCookies(['digital_menu_user'])

  const onSubmit = (values: InitialValues, { resetForm }:any) => {
    editMenu(cookies.digital_menu_user.id, restaurant_id, values.id, {menu: values})
    .then((res: any) => {
      let newMenus = [...Menus]
      console.log(newMenus)
      newMenus[index] = res.data.data
      setMenus(newMenus)
    })
    .catch(err=>console.log(err))
    resetForm()
  }
  return(
    <Formik
      validationSchema={Validations}
      initialValues={values}
      onSubmit={onSubmit}
    >
      {({
        values,
        errors,
        handleChange,
        handleSubmit,
        isSubmitting,
        setFieldValue,
      }) => (
        <Form 
          values={values} 
          errors={errors} 
          handleChange={handleChange} 
          handleSubmit={handleSubmit}
          setFieldValue={setFieldValue} />
        )
      }
    </Formik>
  )
}