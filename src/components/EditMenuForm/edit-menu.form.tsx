import { TextField, Button, Select, FormControl, MenuItem, InputLabel, FormHelperText } from '@material-ui/core'
import { SupervisedUserCircle, PhotoCamera } from '@material-ui/icons'
import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next';

import './edit-menu.scss'

export const Form = ({ values, errors, handleChange, handleSubmit, setFieldValue }:any) => {
  const [Image, setImage] = useState('')
  useEffect(() => {
    setImage(values.image)
  }, [values])
  const { t } = useTranslation()

  return(
    <form className="form-container">
      <div className="restaurant-form">
        <div className="restaurant-form__section">
          <TextField 
            value={values.name}
            helperText={errors.name}
            onChange={handleChange('name')}
            label={t('MENU_NAME')} 
            error={errors.name? true : false }
          />
          <FormControl 
            className="menu__select"
            error={errors.type? true : false }
          >
            <InputLabel>{t('MENU_TYPE')}</InputLabel>
            <Select
              value={values.type}
              onChange={handleChange('type')}
            >
              <MenuItem value="">
                -- Selecciona una opción --
              </MenuItem>
              <MenuItem value="Comida">Comida</MenuItem>
              <MenuItem value="Bebida">Bebida</MenuItem>
            </Select>
            <FormHelperText>{errors.type}</FormHelperText>
          </FormControl>
        </div>
        <div className="restaurant-form__section">
          <input accept="image/*" id="menu-upload" type="file" onChange={(event) => {
            let reader = new FileReader();
            // @ts-ignore
            reader.readAsDataURL(event.currentTarget.files[0]);
            reader.onload = () => {
              // @ts-ignore
              setFieldValue("image", reader.result);
              // @ts-ignore
              setImage(reader.result)
            }
            reader.onerror = (error) => {
              console.log('Error: ', error);
            }
          }} />
          <label htmlFor="menu-upload">
            <Button endIcon={<PhotoCamera />} component="span">
              {t('FORM_IMAGEBUTTON')}
            </Button>
          </label>
          {Image !== ''? 
            <div className="form-container__image-wrap">
              <span>&times;</span>
              <img alt="restaurant" src={Image} width={'100%'} />
            </div>
          :null}
        </div>
      </div>
      <div className="submitButton">
        <Button 
          endIcon={<SupervisedUserCircle />}
          onClick={handleSubmit} 
        >
          {t('LOGIN_BUTTON')}
        </Button>
      </div>
    </form>
  )
}