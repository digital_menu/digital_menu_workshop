import * as Yup from 'yup'
import i18n from 'i18next'

export const Validations = Yup.object({
  name: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED')),
  type: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED')),
})