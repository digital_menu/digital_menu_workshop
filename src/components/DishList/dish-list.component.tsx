import { useCookies } from 'react-cookie'
import { useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import React, { useEffect, useState } from 'react'
import { Restaurant, Delete } from '@material-ui/icons'
import { Avatar, FormControlLabel, IconButton, List, ListItem, ListItemIcon, ListItemAvatar, ListItemText, Dialog, Switch, DialogTitle } from '@material-ui/core'

import { getDishes } from '../../services'
import { DishForm, DeletingModal } from '../../components'

import './dish-list.scss'

export const DishList = ({ menu_id, section_id }:any) => {
  const [isDeleting, setisDeleting] = useState(false)
  const [cookies] = useCookies(['digital_menu_user']);
  const [delItem, setdelItem] = useState(null)
  const [isOpen, setisOpen] = useState(false)
  const [Dishes, setDishes] = useState([])
  const { restaurant_id } = useParams();
  const { t } = useTranslation()

  useEffect(() => {
    const fetchData = async () => {
      const result = await getDishes(cookies.digital_menu_user.id, restaurant_id, menu_id, section_id)
      setDishes(result.data.data);
    };
    fetchData();
  }, [restaurant_id, cookies.digital_menu_user.id, menu_id, section_id]);

  return(
    <div className="menu-category__item-list">
      <List className="dish__list">
        <div className="dish__add-menu-button">
          <div onClick={() => setisOpen(true)} className="add-button-dish">+</div>
        </div>
        {
          Dishes.map((value: any, index) => (
              <ListItem key={`dish-${index}`} className="dish__list-item">
                {
                  value.image?(
                    <ListItemAvatar className="dish__list-item-image">
                      <img alt="section" src={value.image} width={50} />
                    </ListItemAvatar>
                  ):(
                    <ListItemIcon>
                      <Avatar>
                        <Restaurant />
                      </Avatar>
                    </ListItemIcon>
                  )
                }
                <ListItemText primary={value.name} secondary={value.description} />
                <FormControlLabel
                  control={
                    <Switch
                      checked={value.enabled}
                      onChange={() => alert("cambiando")}
                    />
                  }
                  label={value.enabled? 'Disponible':'No Disponible'}
                />
                <IconButton onClick={() =>{
                    setdelItem(value)
                    setisDeleting(true)
                  }}>
                  <Delete />
                </IconButton>
              </ListItem>
          ))
        }
      </List>
      <Dialog
        open={isDeleting}
        onClose={() => setisDeleting(false)}
      >
        <DeletingModal
          list={Dishes}
          type="Platillo"
          value={delItem}
          menu_id={menu_id}
          section_id={section_id}
          setDeletion={setDishes}
          setisDeleting={setisDeleting}
        />
      </Dialog>
      <Dialog
        open={isOpen}
        onClose={() => setisOpen(false)}
      >
        <DialogTitle className="text-style">{t('RESTAURANT_FORM_TITLE')}</DialogTitle>
        <span className="text-style" style={{ textAlign: 'justify', paddingLeft: 24, paddingRight: 24}}>
          {t('RESTAURANT_FORM_DESCRIPTION')}
        </span>
        <DishForm setDishes={setDishes} menu_id={menu_id} section_id={section_id}/>
      </Dialog>
    </div>
  )
}
