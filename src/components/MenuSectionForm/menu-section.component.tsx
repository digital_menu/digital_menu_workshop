import React from 'react'
import { Formik } from 'formik'
import { useCookies } from 'react-cookie'
import { useParams } from "react-router-dom"

import { Form } from './menu-section.form'
import { Validations } from './validations'
import { addSection } from '../../services'

type InitialValues = {
  name: string
  image: any
  description: string 
}

const initialValues: InitialValues = {
  name: '',
  image: null,
  description: ''
} 

export const MenuSectionForm = ({ setSections, menu_id }: any) => {

  let { restaurant_id } = useParams()
  const [cookies] = useCookies(['digital_menu_user'])
  const onSubmit = (values: InitialValues, { resetForm }:any) => {
    addSection(cookies.digital_menu_user.id, restaurant_id, menu_id, { section: values })
    .then(res => setSections((sections:any) => [...sections, res.data.data]))
    resetForm()
  }
  return(
    <Formik
      validationSchema={Validations}
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      {({
          values,
          errors,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue
        }) => (
        <Form 
          values={values} 
          errors={errors} 
          handleChange={handleChange} 
          handleSubmit={handleSubmit}
          setFieldValue={setFieldValue} />
        )
      }
    </Formik>
  )
}