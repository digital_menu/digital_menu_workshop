import { useCookies } from 'react-cookie'
import { Delete } from '@material-ui/icons'
import { useParams } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import { List, ListSubheader, Button, Dialog, Accordion, AccordionSummary, AccordionDetails, AccordionActions } from '@material-ui/core'

import { getSections } from '../../services'
import { MenuSectionForm, DishList, DeletingModal } from '../../components'

import './menu-section-list.scss'


export const MenuSectionList = ({ menu_id }:any) => {
  const [cookies] = useCookies(['digital_menu_user']);
  const [isDeleting, setisDeleting] = useState(false)
  const [Sections, setSections] = useState([])
  const [delItem, setdelItem] = useState(null)
  const [isOpen, setisOpen] = useState(false)
  let { restaurant_id } = useParams();

  useEffect(() => {
    const fetchData = async () => {
      const result = await getSections(cookies.digital_menu_user.id, restaurant_id, menu_id)
      setSections(result.data.data);
    };
    fetchData();
  }, [restaurant_id, cookies.digital_menu_user.id, menu_id]);

  return(
    <div className="menu-category">
      <List>
        {Sections.map((value: any, index) => (
          <li key={`section-${index}`} className="menu-category__item">
              <Accordion>
                <AccordionSummary className="menu-category__item-title">
                  {value.back_image? 
                    <img alt="section" src={value.back_image} className="menu-category__item-image"/>
                    :null
                  }
                  <ListSubheader color="primary">{value.name}</ListSubheader>
                </AccordionSummary>
                <AccordionDetails className="menu-category__item-title__expanded">
                  <DishList menu_id={menu_id} section_id={value.id} />
                </AccordionDetails>
                <AccordionActions className="menu-category__item-title__expanded">
                  <Button color="primary" onClick={() =>{
                      setdelItem(value)
                      setisDeleting(true)
                    }}>
                    <Delete />{' '}
                    Eliminar Sección
                  </Button>
                </AccordionActions>
              </Accordion>
          </li>
        ))}
      </List>
      <div className="menu-category__add-menu-button">
        <div onClick={() => setisOpen(true)} className="add-button-section">+</div>
      </div>
      <Dialog
        open={isOpen}
        onClose={() => setisOpen(false)}
      >
        <MenuSectionForm setSections={setSections} menu_id={menu_id}/>
      </Dialog>
      <Dialog
        open={isDeleting}
        onClose={() => setisDeleting(false)}
      >
        <DeletingModal setisDeleting={setisDeleting} value={delItem} type="Sección" />
      </Dialog>
    </div>
  )
}
