import { Formik } from 'formik'
import React, { useState } from 'react'
import { useCookies } from 'react-cookie'

import { Form } from './restaurant.form'
import { Validations } from './validations'
import { addRestaurant, addSettings } from '../../services'

type InitialValues = {
  address: string,
  coordinates: string,
  description: string,
  image: string,
  setting: any
  name: string
}

const initialValues: InitialValues = {
  address: '',
  coordinates: '',
  description: '',
  image: '',
  name: '',
  setting: { main_color: "#fff", secondary_color: "#fff", text_color: "#000", secondary_text_color: "#000" }
} 

export const RestaurantForm = ({ setRestaurants, setisOpen }:any) => {
  const [isLoading, setisLoading] = useState(false)
  const [cookies] = useCookies(['digital_menu_user']);
  const onSubmit = (values: InitialValues, { resetForm }:any) => {
    setisLoading(true)
    values.name = values.name.replace(/\s/g, '-') 
    addRestaurant(cookies.digital_menu_user.id, { restaurant: values })
    .then((res: any) => {
      addSettings(cookies.digital_menu_user.id, res.data.data.id, { setting: values.setting })
      setRestaurants((state:any) => [res.data.data, ...state])
      setisLoading(false)
      setisOpen(false)
    })
    .catch(()=>{
      setisLoading(false)
      setisOpen(false)
    })
    resetForm()
  }
  return(
    <Formik
      validationSchema={Validations}
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      {({
          values,
          errors,
          isValid,
          handleChange,
          handleSubmit,
          isSubmitting,
          setFieldValue
        }) => (
          <Form 
            values={values} 
            errors={errors} 
            isValid={isValid}
            isLoading={isLoading}
            setisOpen={setisOpen}
            handleChange={handleChange} 
            handleSubmit={handleSubmit}
            setFieldValue={setFieldValue} />
        )}
    </Formik>
  ) 
}