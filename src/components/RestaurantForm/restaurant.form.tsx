import React, { useState } from 'react'
import { Add, PhotoCamera } from '@material-ui/icons'
import { TextField, Button } from '@material-ui/core'
import { CircularProgress } from '@material-ui/core'

import { useTranslation } from 'react-i18next';

import '../../assets/scss/form.scss'
import './restaurant.scss'

export const Form = ({ values, errors, handleChange, handleSubmit, setFieldValue, isLoading, isValid, setisOpen }:any) => {
  const [Image, setImage] = useState('')
  const { t } = useTranslation()

  return(
    <div className="form-container">
      <div className="restaurant-form">
        <div className="restaurant-form__section">
          <TextField
            color="primary"
            value={values.name}
            disabled={isLoading}
            helperText={errors.name}
            onChange={handleChange('name')}
            label={t('RESTAURANT_FORM_NAMEFIELD')}
            error={errors.name? true : false }
          />
          <TextField
            value={values.description}
            disabled={isLoading}
            helperText={errors.description}
            onChange={handleChange('description')}
            label={t('RESTAURANT_FORM_DESCRIPTIONFIELD')}
            error={errors.description? true : false }
            />
          <TextField
            disabled={isLoading}
            value={values.address}
            helperText={errors.address}
            onChange={handleChange('address')}
            label={t('RESTAURANT_FORM_ADDRESSFIELD')}
            error={errors.address? true : false }
          />
        </div>
        <div className="restaurant-form__section">
          <input accept="image/*" id="menu-upload" type="file" onChange={(event) => {
            let reader = new FileReader();
            // @ts-ignore
            reader.readAsDataURL(event.currentTarget.files[0]);
            reader.onload = () => {
              // @ts-ignore
              setImage(reader.result)
              // @ts-ignore
              setFieldValue("image", reader.result);
            }
            reader.onerror = (error) => {
              console.log('Error: ', error);
            }
            event.target.value = ''
          }} />
          {Image !== ''?
            <div className="form-container__image-wrap">
              <span onClick={()=>{
                setImage('')
              }}>&times;</span>
              <img alt="restaurant" src={Image} width={'100%'} />
            </div>:null}
          <label htmlFor="menu-upload" >
            <Button endIcon={<PhotoCamera />} color="primary" disabled={isLoading} component="span">
              {t('FORM_IMAGEBUTTON')}
            </Button>
          </label>
        </div>
      </div>
      <div className="submitButton">
        {
          isLoading?
          <CircularProgress />:null
        }
        <Button
          color="primary"
          endIcon={<Add />}
          disabled={isLoading || !isValid}
          onClick={
            ()=>{
              setImage('')
              handleSubmit()
            }}
        >
          {t('RESTAURANT_FORM_SUBMITBUTTON')}
        </Button>
        <Button style={{ color: '#f44336' }} onClick={()=> setisOpen(false)}>
          {t('CANCEL_BUTTON')}
        </Button>
      </div>
    </div>
  )
}
