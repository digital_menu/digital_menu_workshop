import * as Yup from 'yup'
import i18n from 'i18next'

export const Validations = Yup.object({
  name: Yup.string()
    .max(20, i18n.t('FORMFIELD_20_LENGTH'))
    .required(i18n.t('FORMFIELD_REQUIRED')),
  description: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED')),
  address: Yup.string()
    .required(i18n.t('FORMFIELD_REQUIRED'))
})