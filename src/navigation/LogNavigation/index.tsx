import React from 'react'
import { Switch, Route } from "react-router-dom";

import { Login } from '../../views'
import { OnSignInNavigation } from '../onSignInNavigation'

export const LogNavigation = () => {
  return (
    <Switch>
      <Route path="/dashboard">
        <OnSignInNavigation />
      </Route>
      <Route path="/">
        <Login />
      </Route>
    </Switch>
  )
}