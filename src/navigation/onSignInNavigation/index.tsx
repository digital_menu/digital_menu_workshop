import React from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom";

import { Dashboard, MenuWorkshop } from '../../views'

export const OnSignInNavigation = () => {
  let { path } = useRouteMatch();
  return (
    <Switch>
      <Route exact path={path}>
        <Dashboard />
      </Route>
      <Route path={`${path}/:restaurant_id/menu-workshop`}>
        <MenuWorkshop />
      </Route>
    </Switch>
  )
}