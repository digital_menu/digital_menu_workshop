import { ThemeProvider, createMuiTheme } from '@material-ui/core'
import { BrowserRouter as Router } from "react-router-dom"
import React from 'react'

import { LogNavigation } from './navigation'
import './assets/scss/text.scss'
import './App.scss';

const theme = createMuiTheme({
	palette: {
	  primary: {
      main: '#21295c',
      contrastText: '#fff'
	  },
	  secondary: {
      main: '#1b3b6f',
      contrastText: '1c7293'
	  }
	}
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <div className="App">
          <LogNavigation />
        </div>
      </Router>
    </ThemeProvider>
  );
}

export default App;
