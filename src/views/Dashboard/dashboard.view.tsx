import { AppBar, Typography, Dialog, DialogTitle, Button, Toolbar, useTheme } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useCookies } from 'react-cookie'

import { getRestaurants } from '../../services'
import { RestaurantCard, RestaurantForm } from '../../components'
import './dashboard.scss'

export const Dashboard = () => {
  const [cookies] = useCookies(['digital_menu_user']);
  const [Restaurants, setRestaurants] = useState([])
  const [isOpen, setisOpen] = useState(false)
  const { t } = useTranslation()
  const theme = useTheme()

  useEffect(() => {
    const fetchData = async () => {
      const result = await getRestaurants(cookies.digital_menu_user.id)
      setRestaurants(result.data.data);
    };
    fetchData();
  }, [cookies]);

  return(
    <div className="dashboard">
      <AppBar color="secondary" className="dashboard__header" position="static">
        <Toolbar>
          <Typography  variant="h6" style={{ flexGrow: 1, color: theme.palette.primary.contrastText }}>
            {t('APPBAR_TITLE')}
          </Typography>
          <Button style={{ color: theme.palette.primary.contrastText }}>
            {t('APPBAR_LOGOUT')}
          </Button>
        </Toolbar>
      </AppBar>
      <Typography variant="h5" className="dashboard__title">
        {t('DASHBOARD_TITLE')}
      </Typography>
      <Typography variant="body2" className="dashboard__description">
        {t('DASHBOARD_DESCRIPTION')}
      </Typography>
      <div className="dashboard__container" >
        <div className="dashboard__restaurant-cards">
          <div className="dashboard__add-menu-button">
            <div onClick={() => setisOpen(true)} className="add-button-restaurant">+</div>
          </div>
          {
          Restaurants.map((value: any, index)=>(
            <RestaurantCard value={value} key={index+'_restaurant-card'} />
          ))
          }
        </div>
      </div>
      <Dialog
        open={isOpen}
        onClose={() => setisOpen(false)}
      >
        <DialogTitle className="text-style">{t('RESTAURANT_FORM_TITLE')}</DialogTitle>
        <span className="text-style" style={{ textAlign: 'justify', paddingLeft: 24, paddingRight: 24}}>
          {t('RESTAURANT_FORM_DESCRIPTION')}
        </span>
        <RestaurantForm setisOpen={setisOpen} setRestaurants={setRestaurants}/>
      </Dialog>
    </div>
  )
}
