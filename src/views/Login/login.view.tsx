import React from 'react'

import { LoginForm } from '../../components'
import './login.scss'

export const Login = () => {
  return(
    <div style={{ backgroundImage: require('../../assets/images/menu.jpg') }} className="login">
      <LoginForm />
    </div>
  )
}