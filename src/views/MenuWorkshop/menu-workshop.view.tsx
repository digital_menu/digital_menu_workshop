import { useCookies } from 'react-cookie'
import { useParams } from "react-router-dom"
import { Button, Dialog, DialogTitle } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { AddOutlined, Edit, Menu, Image } from '@material-ui/icons'

import { EditMenuForm, MenuForm, MenuSectionList, ImageModal } from '../../components'
import { getMenus } from '../../services'

import './menu-workshop.scss'

export const MenuWorkshop = () => {
  let { restaurant_id } = useParams();
  const [isOpenImage, setisOpenImage] = useState(false)
  const [cookies] = useCookies(['digital_menu_user'])
  const [TabIndex, setTabIndex] = useState(0)
  const [isOpen, setisOpen] = useState(false)
  const [isEdit, setIsEdit] = useState(false)
  const [Menus, setMenus] = useState([])
  const { t } = useTranslation()

  useEffect(() => {
    const fetchData = async () => {
      const result = await getMenus(cookies.digital_menu_user.id, restaurant_id)
      setMenus(result.data.data);
    };
    fetchData();
  }, [restaurant_id, cookies.digital_menu_user.id]);

  return(
    <div>
      <div className="appbar">
        <div>
          {Menus.map((value:any, index)=>(
            <Button className="appbar__tab-button" key={index+'_menu_tabs'} onClick={() => setTabIndex(index)} >
              {value.image?
                <img alt="menu" src={value.image} className="appbar__tab-button-image" />:<Menu fontSize="large" />
              }
              <span className="appbar__tab-button-label">{value.name}</span>
            </Button>
          ))}
        </div>
        <Button className="appbar__tab-button" onClick={() => setisOpen(true)} >
          <AddOutlined />
          <span>Crear Sección</span>
        </Button>
      </div>
      <Dialog open={isOpen} onClose={() => setisOpen(false)}>
        <DialogTitle className="text-style">
          {t('MENU_FORM_TITLE')} 
        </DialogTitle>
        <span className="text-style" style={{ textAlign: 'justify', paddingLeft: 24, paddingRight: 24}}>
          {t('MENU_FORM_DESCRIPTION')} 
        </span>
        <MenuForm setisOpen={setisOpen} setMenus={setMenus} />
      </Dialog>
      {Menus.map((value:any , index) => {
        return index === TabIndex? (
          <div className="menu" key={index}>
            <h3 className="dashboard__title">
              {t('WORKSHOP_TITLE')}
            </h3>
            <h4 className="dashboard__description">
              {t('WORKSHOP_DESCRIPTION')}
            </h4>
            <Dialog  open={isOpenImage} onClose={() => setisOpenImage(false)}>
              <ImageModal imageURL={value.image} />
            </Dialog>
            <Dialog open={isEdit} onClose={() => setIsEdit(false)}>
              <EditMenuForm Menus={Menus} index={index} values={value} setMenus={setMenus} />
            </Dialog>
            <h2>{value.name}</h2>
            <h3>{value.description}</h3>
            <Button style={{ marginLeft: 10, marginRight: 10 }} color="primary" disabled={!value.image} onClick={() => setisOpenImage(true)} endIcon={<Image />}>
              {t('WORKSHOP_MENU_IMAGEBUTTON')} 
            </Button>
            <Button style={{ marginLeft: 10, marginRight: 10 }} color="primary" onClick={() => setIsEdit(true)} endIcon={<Edit/>}>
              {t('WORKSHOP_MENU_EDITBUTTON')} 
            </Button>
            <MenuSectionList menu_id={value.id}/>
          </div>
        ):null
      })}
    </div>
  )
}
