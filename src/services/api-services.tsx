import axios from 'axios'

const menuAPI = axios.create( {
  baseURL: 'http://192.168.1.70:4000/api'
});

export const loginUser = async (data: any) =>
  await menuAPI.post('/login', data)

export const getRestaurants = async (user_id: string) =>
  await menuAPI.get(`/users/${user_id}/restaurants`)

export const addRestaurant = async (user_id: string, data: any) =>
  await menuAPI.post(`/users/${user_id}/restaurants`, data)

export const getSettings = async (user_id: string, restaurant_id:string) =>
  await menuAPI.get(`/users/${user_id}/restaurants/${restaurant_id}/settings`)

export const addSettings = async (user_id: string, restaurant_id:string, data: any) =>
  await menuAPI.post(`/users/${user_id}/restaurants/${restaurant_id}/settings`, data)

export const updateSettings = async (user_id: string, restaurant_id: string, setting_id: number, data: any) =>
  await menuAPI.patch(`/users/${user_id}/restaurants/${restaurant_id}/settings/${setting_id}`, data)

export const getPromos = async (user_id: string, restaurant_id:string) =>
  await menuAPI.get(`/users/${user_id}/restaurants/${restaurant_id}/promos`)

export const addPromos = async (user_id: string, restaurant_id:string, data: any) =>
  await menuAPI.post(`/users/${user_id}/restaurants/${restaurant_id}/promos`, data)

export const getMenus = async (user_id: string, restaurant_id:string) =>
  await menuAPI.get(`/users/${user_id}/restaurants/${restaurant_id}/menus`)

export const addMenu = async (user_id: string, restaurant_id:string, data: any) =>
  await menuAPI.post(`/users/${user_id}/restaurants/${restaurant_id}/menus`, data)

export const editMenu = async (user_id: string, restaurant_id:string, menu_id:number, data: any) =>
  await menuAPI.patch(`/users/${user_id}/restaurants/${restaurant_id}/menus/${menu_id}`, data)

export const getSections = async (user_id: string, restaurant_id:string, menu_id:string) =>
  await menuAPI.get(`/users/${user_id}/restaurants/${restaurant_id}/menus/${menu_id}/sections`)

export const addSection = async (user_id: string, restaurant_id:string, menu_id:string, data: any) =>
  await menuAPI.post(`/users/${user_id}/restaurants/${restaurant_id}/menus/${menu_id}/sections`, data)

export const getDishes = async (user_id: string, restaurant_id:string, menu_id:string, section_id: string) =>
  await menuAPI.get(`/users/${user_id}/restaurants/${restaurant_id}/menus/${menu_id}/sections/${section_id}/dishes`)

export const deleteDishes = async (user_id: string, restaurant_id:string, menu_id:string, section_id: string, dish_id: string) =>
  await menuAPI.delete(`/users/${user_id}/restaurants/${restaurant_id}/menus/${menu_id}/sections/${section_id}/dishes/${dish_id}`)

export const addDishes = async (user_id: string, restaurant_id:string, menu_id:string, section_id: string, data: any) =>
  await menuAPI.post(`/users/${user_id}/restaurants/${restaurant_id}/menus/${menu_id}/sections/${section_id}/dishes`, data)
