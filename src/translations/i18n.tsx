import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import { EsMX } from './languages';

const DEFAULT_LANGUAGE = "EsMX"
const resources = {
  EsMX: {
    translation: {
      ...EsMX
    }
  }
}

i18n.use(initReactI18next).init({
  lng: DEFAULT_LANGUAGE,
  resources: resources,
})

export default i18n;