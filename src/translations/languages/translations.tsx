export type Translation = {
  /* 
    General Translations
  */
  CANCEL_BUTTON: string
  DELETE_BUTTON: string
  FORM_IMAGEBUTTON: string
  FORM_OPTIONSELECTION: string
  /* 
    AppBar Translations
  */
  APPBAR_TITLE: string
  APPBAR_LOGOUT: string
  /* 
    Dashboard Translations
  */
  DASHBOARD_TITLE: string
  DASHBOARD_DESCRIPTION: string
  /* 
    Menu Workshop Translations
  */
  WORKSHOP_TITLE: string
  WORKSHOP_DESCRIPTION: string
  WORKSHOP_MENU_EDITBUTTON: string
  WORKSHOP_MENU_IMAGEBUTTON: string
  /* 
    Restaurant Card Translations
  */
  RESTAURANT_CARD_MENUBUTTON: string
  /* 
    Restaurant Form Translations
  */
  RESTAURANT_FORM_TITLE: string
  RESTAURANT_FORM_DESCRIPTION: string
  RESTAURANT_FORM_NAMEFIELD: string
  RESTAURANT_FORM_DESCRIPTIONFIELD: string
  RESTAURANT_FORM_ADDRESSFIELD: string
  RESTAURANT_FORM_SUBMITBUTTON: string
  /* 
    Menu Form Translations
  */
  MENU_FORM_TITLE: string
  MENU_FORM_DESCRIPTION: string
  MENU_FORM_NAMEFIELD: string
  MENU_FORM_TYPEFIELD: string
  MENU_FORM_SUBMITBUTTON: string
  /* 
    Menu Section Form Translations
  */
  /* 
    Dish Form Translations
  */
  /* 
    Delete Dialog
  */
  DELETE_DIALOG_TITLE: string
  DELETE_DIALOG_DESCRIPTION: string

  FORMFIELD_REQUIRED: string
  FORMFIELD_20_LENGTH: String
  FORMFIELD_30_LENGTH: String
  FORMFIELD_50_LENGTH: String
  FORMFIELD_70_LENGTH: String
  
  LOGIN_BUTTON: String
  LOGIN_USERNAME: String
  LOGIN_PASSWORD: String
  
  RESTAURANTFORM_NAME: String
  RESTAURANTFORM_DESCRIPTION: String
  RESTAURANTFORM_ADDRESS: String
  RESTAURANTFORM_COORDINATES: String
  RESTAURANTFORM_IMAGE: String

  DISHFORM_DISH_NAME: String
  DISHFORM_DISH_PRICE: String
  DISHFORM_DISH_BUTTON: String
  DISHFORM_DISH_DESCRIPTION: String

  SECTIONFORM_SECTION_NAME: String
  SECTIONFORM_SECTION_BUTTON: String
  SECTIONFORM_SECTION_DESCRIPTION: String
}