import { Translation } from './translations'

export const EsMX: Translation  = {
  /* 
    General Translations
  */
  CANCEL_BUTTON: 'Cancelar',
  DELETE_BUTTON: 'Sí, eliminar',
  FORM_IMAGEBUTTON: 'Añadir Imagen',
  FORM_OPTIONSELECTION: '-- Selecciona una opción --',
  /* 
    AppBar Translations
  */
  APPBAR_TITLE: 'Digital Menu Workshop',
  APPBAR_LOGOUT: 'Cerrar Sesión',
  /* 
    Dashboard Translations
  */
  DASHBOARD_TITLE: 'Panel de administración',
  DASHBOARD_DESCRIPTION: 'Todos los restaurantes configurados se mostraran aquí, editalos o crea los menus',
  /* 
    Menu Workshop Translations
  */
  WORKSHOP_TITLE: 'Editor de menus',
  WORKSHOP_DESCRIPTION: 'Revisa, agrega, modifica o elimina elementos de tu menú',
  WORKSHOP_MENU_EDITBUTTON: 'Editar menú',
  WORKSHOP_MENU_IMAGEBUTTON: 'Imagen de menú',  
  /* 
    Restaurant Card Translations
  */
  RESTAURANT_CARD_MENUBUTTON: 'Crea tu Menú',
  /* 
    Restaurant Form Translations
  */
  RESTAURANT_FORM_TITLE: 'Añade un nuevo restaurante',
  RESTAURANT_FORM_DESCRIPTION: 'Crea el restaurante que tendra el nuevo menu digital',
  RESTAURANT_FORM_NAMEFIELD: 'Escribe el nombre ...',
  RESTAURANT_FORM_DESCRIPTIONFIELD: 'Escribe la descripción ...',
  RESTAURANT_FORM_ADDRESSFIELD: 'Escribe la dirección ...',
  RESTAURANT_FORM_SUBMITBUTTON: 'Añadir Restaurante',
  /* 
    Menu Form Translations
  */
  MENU_FORM_TITLE: 'Agregar nuevo menu',
  MENU_FORM_DESCRIPTION: 'Escribe el nombre de tu nuevo menu y su tipo',
  MENU_FORM_NAMEFIELD: 'Escribe el nombre ...',
  MENU_FORM_TYPEFIELD: 'Selecciona el tipo de menú ... ',
  MENU_FORM_SUBMITBUTTON: 'Agregar menú',
  /* 
    Delete Dialog
  */
  DELETE_DIALOG_TITLE: 'Eliminar',
  DELETE_DIALOG_DESCRIPTION: '¿Deseas eliminar este elemento?',

  FORMFIELD_REQUIRED: 'Campo requerido',
  FORMFIELD_20_LENGTH: 'Máx 20 cáracteres',
  FORMFIELD_30_LENGTH: 'Máx 30 cáracteres',
  FORMFIELD_50_LENGTH: 'Máx 50 cáracteres',
  FORMFIELD_70_LENGTH: 'Máx 70 cáracteres',

  LOGIN_BUTTON: 'Iniciar sesión',
  LOGIN_USERNAME: 'Usuario',
  LOGIN_PASSWORD: 'Contraseña',

  RESTAURANTFORM_NAME: 'Nombre...',
  RESTAURANTFORM_DESCRIPTION: 'Descripción...',
  RESTAURANTFORM_ADDRESS: 'Dirección...',
  RESTAURANTFORM_COORDINATES: 'Coordernadas...',
  RESTAURANTFORM_IMAGE: 'Imagen...',

  DISHFORM_DISH_NAME: 'Nombre del platillo',
  DISHFORM_DISH_PRICE: 'Precio del platillo',
  DISHFORM_DISH_BUTTON: 'Agregar nuevo platillo',
  DISHFORM_DISH_DESCRIPTION: 'Descripción del platillo',

  SECTIONFORM_SECTION_NAME: 'Nombre de la sección',
  SECTIONFORM_SECTION_BUTTON: 'Agregar nueva sección',
  SECTIONFORM_SECTION_DESCRIPTION: 'Descripción de la sección',
}